def add(a, b):
    print(f"ADDING {a} + {b}")
    return a + b

def substruct(a, b):
    print(f"SUBSTRUCTING {a} - {b}")
    return a - b 

def  multiply(a, b):
    print(f"MULTIPLYING {a} * {b}")
    return a * b 

def divide(a, b):
    print(f"DIVIDING {a} / {b}")
    return a / b

print("Let's do some math with just functions")

age = add(30, 5)
height = substruct(78, 4)
weight = multiply(90, 2)
iq = divide(100, 2)

print(f"Age {age}, Height {height}, Weight {weight}, IQ {iq}")

#a puzzle for the extra credit, type it anyway
print("here is a puzzle")
what = add(age, substruct(height, multiply(weight, divide(iq, 2))))
print("that becomes:", what, "can you do it by hand?")