#!/usr/bin/#!/usr/bin/env python3

my_name = 'Drorit Zilberberg'
my_age = 39 #not a lie
my_height = 165 #cm
my_weight = 100 #kg
my_eyes = 'Grey'
my_teeth = 'White'
my_hair = 'Carrot'

print(f"Let's talk about {my_name}.")
print(f"She's {my_height} cm tall. This is", my_height * 0.394, "inches.")
print(f"She's {my_weight} kg heavy. This is", my_weight * 2.205, "pounds.")
print("Actually it's very heavy.")
print(f"She's got {my_eyes} eyes and {my_hair} hair.")
print(f"Her teeth are usually {my_teeth} depending on the tea.")

#this line is tricky, will try to get it exactly right.
total = my_age + my_height + my_weight
print(f"If I add {my_age}, {my_height}, and {my_weight} I get {total}.")
