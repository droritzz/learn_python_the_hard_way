# script to copy one file to another

from sys import argv
from os.path import exists # this returns TRUE if a file exists, based on its name in a string as an argument

script, from_file, to_file = argv

print(f"copying from {from_file} to {to_file}")

in_file = open(from_file)
indata =  in_file.read()

print(f"the input file is {len(indata)} bytes long") #gets the length of the string that I pass to it, returns as a number

print(f"does the output file exist? {exists(to_file)}")
print("ready, hit RETURN to continue, CTRL-C to abort.")
input()

out_file = open(to_file, "w")
out_file.write(indata)

print("alright, all done")

out_file.close()
in_file.close()
