#!/usr/bin/env python3
from sys import argv
script, user_name = argv
prompt = '> '

print(f"hi {user_name}, I am the {script} script.")
print("I'd like do ask you a few questions.")
print(f"do you like me {user_name}?")
likes = input(prompt)

print(f"where do you live {user_name}?")
lives = input(prompt)

print("what kind of comuter do you have?")
computer = input(prompt)

print(f"""
alright, so you said {likes} about liking me.
and you live in {lives}. not sure where that is.
and you have a {computer} computer. nice
""")
