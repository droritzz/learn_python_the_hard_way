#!/usr/bin/#!/usr/bin/env python3
#setting a var
types_of_people = 10
#setting a var, formatting a string, putting a var(string) inside of a string
x = f"There are {types_of_people} types of people."
binary = "binary"
do_not = "don't"
#setting a string, formatting a string, putting another strings inside of it
y = f"Those who know {binary} and those who {do_not}."
#printing two strings with formatting and containg another strings
print(x)
print(y)

print(f"I said: {x}.")
print(f"I also said: '{y}'")
hilarious = False
joke_evaluation = "Isn't that joke so funny?! {}"

print(joke_evaluation.format(hilarious))

w = "This is the left side of ..."
e = "a string with a right side."

print (w + e)
