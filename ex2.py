#!/usr/bin/env python3

# a comment, this is so you can read your programm later
#Anything after the # is ignored by python3
print("I could have code like this") #and the comment after is ignored
#you can also use a comment to "disable" or comment out code:
#print("this will not run")
print("this will run")
