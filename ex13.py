#!/usr/bin/env python3
from sys import argv # argv - argument variable, holds the arguments you pass to the script while running it

script, first, second, third = argv
print("the script is called:", script)
print("the first variable is :", first)
# name = input("please enter your name:" )
# print("the wild variable is",name)
print("the second variable is :", second)
print("the third variable is :", third)
