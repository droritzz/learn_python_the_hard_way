#!/usr/bin/env python3
print("How old are you?", end=' ') #end=' ' tell the print command to not end the line with a newline character and go to the next line.
age = input()
print("How tall are you?", end=' ')
height = input()
print("How much do you weight?", end=' ')
weight = input()
print(f"so you're {age} old, {height} tall and {weight} heavy")
