#!/usr/bin/env python3
# get the file name
from sys import argv
script, filename = argv
#open the file. takes the parameter and returns avalue
txt = open(filename)
#call the read function on the txt variable.
print(f"here is your file {filename}:")
print(txt.read())
#prompt the file name again
print("type the filename again: ")
file_again = input("> ")
#open function with the parameter of the prompt
txt_again = open(file_again)
#read function on the new variable
print(txt_again.read())
# close the files after opening them!! it's very important!!
txt.close()
txt_again.close()
